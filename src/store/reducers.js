import { combineReducers } from 'redux';
import { loginStatusReducer } from './LoginStatus/reducers'

export default combineReducers({
    loginStatus: loginStatusReducer
});