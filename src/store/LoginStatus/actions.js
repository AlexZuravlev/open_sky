export const GET_LOGIN_STATUS = 'GET_LOGIN_STATUS';

export const setCurrentLoginStatus = loggedIn => {

    return ({
        type: GET_LOGIN_STATUS,
        payload: loggedIn
    })};
