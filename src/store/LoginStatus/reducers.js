import {GET_LOGIN_STATUS} from './actions';

const initialState = {
    loggedIn: false
};

export const loginStatusReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_LOGIN_STATUS:
            return {
                ...state,
                loggedIn: action.payload
            };
        default:
            break
    }

    return state;
};
