import React from 'react';
import './LoginPage.scss';
import {Redirect} from 'react-router-dom'
import LoginForm from "../../containers/LoginForm/LoginForm";
import { connect } from 'react-redux';

class LoginPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            logged: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.loginStatus !== this.props.loginStatus) {
            this.setState({logged: this.props.loginStatus})
        }
    }

    render() {
        if(this.state.logged){
            return <Redirect to='/'/>
        }
        return  (
            <LoginForm/>
        )
    }
}

export default connect(({loginStatus}) => ({
    loginStatus: loginStatus.loggedIn
}))(LoginPage)
