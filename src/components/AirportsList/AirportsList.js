import React from "react";
import AirportsCity from "./AirportsCity/AirportsCity";
import {Col, Row} from 'react-bootstrap';
import './airports-list.scss'
import AirportItem from "./AirportItem/AirportItem";
import AirportModal from "../../containers/AirportModal/AirportModal";
import axios from 'axios';
import Config from '../../config'

const Airports = [
    {
        label: 'London Airports',
        airports: [
            {name: 'Heathrow Airport', airportICAO: 'EGLL'},
            {name: 'Gatwick Airport', airportICAO: 'EGKK'},
            {name: 'Luton Airport', airportICAO: 'EGGW'},
            {name: 'Stansted Airport', airportICAO: 'EGSS'}
        ]
    },
    {
        label: 'New York Airports',
        airports: [
            {name: 'John F. Kennedy International Airport', airportICAO: 'KJFK'},
            {name: 'LaGuardia Airport', airportICAO: 'KLGA'},
            {name: 'Newark Airport', airportICAO: 'KEWR'}
        ]
    },
    {
        label: 'Paris Airports',
        airports: [
            {name: 'Charles de Gaulle Airport', airportICAO: 'LFPG'},
            {name: 'Orly Airport', airportICAO: 'LFPO'},
            {name: 'Beauvais Airport', airportICAO: 'LFOB'}
        ]
    },
    {
        label: 'Singapore Airports',
        airports: [
            {name: 'Changi Airport', airportICAO: 'WSSS'},
            {name: 'Seletar Airport', airportICAO: 'WSSL'}
        ]
    },
    {
        label: 'Istanbul Airports',
        airports: [
            {name: 'Ataturk Airport', airportICAO: 'LTBA'},
            {name: 'Sabiha Airport', airportICAO: 'LTFJ'}
        ]
    }
];

class AirportsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            airportName: '',
            airportICAO: '',
            airports: Airports,
            arrivalArr: [],
            departureArr: [],
            startTime: 0,
            endTime: 0,
            requestArrivalMassage: '',
            requestDepartureMassage: '',
            isLoading: true,
            timeInterval: '1'
        }
    }

    getAirports = () => {
        return this.state.airports.map((value, index) => {
            return (
                <Col key={index}>
                    <AirportsCity text={value.label}/>
                    {value.airports.map((item, index) => {
                        return (
                            <AirportItem
                                key={index} text={item.name}
                                click={() => this.setOptions(true, item.name, item.airportICAO)}
                            />
                        )
                    })}
                </Col>
            )
        });

    };

    getTime = (hours) => {
        const timeNow = Date.now();
        const convertedTime = parseInt(hours) * 3600;
        const startTime = Math.round(timeNow / 1000) - convertedTime;
        let endTime = startTime + convertedTime;
        return {
            startTime,
            endTime
        }
    };

    setOptions = (showModal, airportName, airportICAO) => {
        const {timeInterval} = this.state;
        const time = this.getTime(timeInterval);
        this.setState({
            showModal,
            airportName,
            airportICAO
        });
        this.getArrivalData(time.startTime, time.endTime, airportICAO);
        this.getDepartureData(time.startTime, time.endTime, airportICAO);
    };

    getArrivalData = (startTime, endTime, airportICAO) => {
        axios.get(`${Config.apiHost}/flights/arrival?airport=${airportICAO}&begin=${startTime}&end=${endTime}`)
            .then(res => {
                const arrivalArr = res.data;
                this.setState({arrivalArr});
            }).catch(res => {
            this.setState({
                requestArrivalMassage: 'Sorry, we don\'t have flights in this time'
            })
        });
    };

    getDepartureData = (startTime, endTime, airportICAO) => {
        axios.get(`${Config.apiHost}/flights/departure?airport=${airportICAO}&begin=${startTime}&end=${endTime}`)
            .then(res => {
                const departureArr = res.data;
                this.setState({departureArr});
            }).catch(res => {
            this.setState({
                requestDepartureMassage: 'Sorry, we don\'t have flights in this time interval'
            })
        });
    };

    clearState=()=>{
        this.setState({
            arrivalArr: [],
            departureArr: [],
            requestArrivalMassage: '',
            requestDepartureMassage: ''
        });
    };

    setTimeInterval = (event) => {
        const {airportICAO} = this.state;
        const timeInterval = event.target.value;
        const time = this.getTime(timeInterval);
        this.clearState();
        this.getArrivalData(time.startTime, time.endTime, airportICAO);
        this.getDepartureData(time.startTime, time.endTime, airportICAO);
    };
    onModalHide = () => {
        this.setState({
            showModal: false
        });
        this.clearState();
    };

    render() {
        const {showModal, airportName, airportICAO, requestArrivalMassage, requestDepartureMassage, arrivalArr, departureArr} = this.state;
        this.getAirports();
        return (
            <div className='airports-list'>
                <Row>
                    {this.getAirports()}
                </Row>
                <AirportModal
                    show={showModal}
                    onHide={this.onModalHide}
                    name={airportName}
                    icao={airportICAO}
                    arrivalError={requestArrivalMassage}
                    departureError={requestDepartureMassage}
                    arrivals={arrivalArr}
                    departures={departureArr}
                    setTimeInterval={this.setTimeInterval}
                />
            </div>
        )
    }
}

export default AirportsList;