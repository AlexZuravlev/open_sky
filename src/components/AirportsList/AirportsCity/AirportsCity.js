import React from "react";
import './airports-city.scss'

const AirportsCity = (props) => {
    const {text} = props;
    return(
        <div className='airports-city' >
            <span>
                { text }
            </span>
        </div>
    )
};

export default AirportsCity;