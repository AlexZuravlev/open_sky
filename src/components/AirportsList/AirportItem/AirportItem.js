import React from "react";
import './airport-item.scss'

const AirportItem = (props) => {
    const {text, click} = props;
    return(
        <div className='airport-item' onClick={click}>
            <span>
                { text }
            </span>
        </div>
    )
};

export default AirportItem;