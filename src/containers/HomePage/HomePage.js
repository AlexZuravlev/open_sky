import React from 'react'
import {Redirect} from "react-router-dom";
import { connect } from "react-redux";
import AirportsList from "../../components/AirportsList/AirportsList";

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: ''
        }
    }
    render() {
        const {loginStatus} = this.props;
        if (!loginStatus) {
            return <Redirect to='/login'/>
        }
        return (
            <div>
                <AirportsList/>
            </div>
        )
    }
}

export default connect(({loginStatus})=>({
    loginStatus: loginStatus.loggedIn
}))(HomePage);