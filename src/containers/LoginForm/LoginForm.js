import React from 'react';
import {Form, Button} from 'react-bootstrap';
import {setCurrentLoginStatus} from "../../store/LoginStatus/actions";
import {connect} from 'react-redux';


class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }

    componentDidMount() {
        if (localStorage.getItem('loginStatus') === 'true') {
            this.props.setCurrentLoginStatus(true);
        }
    }

    render() {
        return (
            <div className='login-page'>
                <h1>Login</h1>
                <Form className='login-form'>
                    <Form.Group controlId="formUsername" onChange={this.getUsername}>
                        <Form.Control type="text" placeholder="Enter Username"/>
                    </Form.Group>

                    <Form.Group controlId="formPassword" onChange={this.getPassword}>
                        <Form.Control type="password" placeholder="Password"/>
                    </Form.Group>
                    <Button
                        variant="primary"
                        type="submit"
                        className='login_button'
                        onClick={this.getFormDate}
                    >
                        Submit
                    </Button>
                </Form>
            </div>
        )
    }

    getPassword = (event) => {
        this.setState({
            password: event.target.value
        });
    };

    getUsername = (event) => {
        this.setState({
            username: event.target.value
        });
    };

    getFormDate = (event) => {
        event.preventDefault();
        const {password, username} = this.state;
        this.validateFormDate(password, username)
    };

    validateFormDate = (password, username) => {

        if (password === '' || username === '') {
            alert('Input Form Date')
        } else if (password === 'demo' && username === 'demo') {
            localStorage.setItem('loginStatus', 'true');
            this.props.setCurrentLoginStatus(true);
            console.log(document.cookie)
        } else {
            alert('Incorrect data')
        }
    }
}

export default connect(null, {
    setCurrentLoginStatus
})(LoginForm)
