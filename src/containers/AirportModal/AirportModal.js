import React from "react";
import {Modal, Form} from 'react-bootstrap';
import './airport-modal.scss';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class AirportModal extends React.Component {

    render() {
        const {name, icao, setTimeInterval, arrivals, departures, departureError, arrivalError} = this.props;
        return (
            <Modal
                {...this.props}
                size='xl'
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {`${name} (${icao})`}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='modal-grid'>
                        <h4 className='flights-header__main-text'>Arrival Flights in last</h4>
                        <Form>
                            <Form.Group controlId="flights-header__time-interval" onChange={setTimeInterval}>
                                <Form.Control as="select">
                                    <option value='1'>1 hour</option>
                                    <option value='2'>2 hours</option>
                                    <option value='5'>5 hours</option>
                                    <option value='24'>1 day</option>
                                    <option value='48'>2 days</option>
                                </Form.Control>
                            </Form.Group>
                        </Form>
                        <ReactTable
                            data={arrivals}
                            showPagination={true}
                            minRows={0}
                            defaultPageSize={2000}
                            noDataText={arrivalError}
                            showPagination={false}
                            style={{
                                height: "250px"
                            }}
                            className="-striped -highlight"
                            columns={[
                                {
                                    Header: 'Name',
                                    columns: [
                                        {
                                            Header: 'ICAO 24',
                                            accessor: 'icao24'
                                        },
                                        {
                                            Header: 'Call Sign',
                                            accessor: 'callsign'
                                        }
                                    ]
                                },
                                {
                                    Header: 'Info',
                                    columns: [
                                        {
                                            Header: 'Arrival Airport',
                                            accessor: 'estArrivalAirport'
                                        },
                                        {
                                            Header: 'Departure Airport',
                                            accessor: 'estDepartureAirport'
                                        }
                                    ]
                                }
                            ]}
                        />
                    </div>
                    <div className='modal-grid'>
                        <h4 className='grid__header'>Departured Flights</h4>
                        <ReactTable
                            data={departures}
                            showPagination={false}
                            minRows={0}
                            defaultPageSize={2000}
                            noDataText={departureError}
                            style={{
                                height: "250px"
                            }}
                            className="-striped -highlight"
                            columns={[
                                {
                                    Header: 'Name',
                                    columns: [
                                        {
                                            Header: 'ICAO 24',
                                            accessor: 'icao24'
                                        },
                                        {
                                            Header: 'Call Sign',
                                            accessor: 'callsign'
                                        }
                                    ]
                                },
                                {
                                    Header: 'Info',
                                    columns: [
                                        {
                                            Header: 'Arrival Airport',
                                            accessor: 'estArrivalAirport'
                                        },
                                        {
                                            Header: 'Departure Airport',
                                            accessor: 'estDepartureAirport'
                                        }
                                    ]
                                }
                            ]}
                        />
                    </div>
                </Modal.Body>
            </Modal>
        )
    }
}

export default AirportModal;