import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.scss';
import {BrowserRouter as Router, Route} from "react-router-dom";
import LoginPage from "./components/LoginPage/LoginPage";
import HomePage from "./containers/HomePage/HomePage";
import {createStore} from "redux";
import {Provider} from 'react-redux';
import rootReducer from './store/reducers';

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Route
                        exact path='/'
                        render={props => <HomePage/>}
                    />
                    <Route
                        path={'/login'}
                        render={props => <LoginPage/>}
                    />
                </Router>
            </Provider>
        )
    }
}

export default App;
